<?php

global $onetone_animated;
$i                   = 4 ;
$section_title       = onetone_option( 'section_title_'. $i );
$section_menu        = onetone_option( 'menu_title_' . $i );
$parallax_scrolling  = onetone_option( 'parallax_scrolling_' . $i );
$section_css_class   = onetone_option( 'section_css_class_' . $i );
$section_content     = onetone_option( 'section_content_' . $i );
$full_width          = onetone_option( 'full_width_' . $i );
 
$content_model       = onetone_option( 'section_content_model_'. $i , 1 );
$section_subtitle    = onetone_option( 'section_subtitle_' . $i );
$color               = onetone_option( 'section_color_' . $i );
$columns             = absint(onetone_option( 'section_team_columns', 4 ));
$col                 = $columns > 0 ? 12 / $columns : 3;
 
if ( !isset($section_content) || $section_content == '' ) $section_content = onetone_option( 'sction_content_' . $i );
  
$section_id          = sanitize_title( onetone_option( 'menu_slug_' . $i, 'section-' . ($i + 1 ) ));

if ( $section_id == '' ) $section_id = 'section-' . ($i + 1);
  
$section_id          = strtolower($section_id);
  
$container_class = 'container';

if ( $full_width == 'yes' ) $container_class = '';

if ( $parallax_scrolling == 'yes' || $parallax_scrolling == '1' ) $section_css_class  .= ' onetone-parallax'; 

// block customization

$our_team = new WP_Query(['category_name' => 'komanda', 'posts_per_page' => 4]);

$category_id = get_cat_ID('Команда');
$category_link = get_category_link( $category_id );
  
?>
<section id="<?= $section_id ?>" class="home-section-<?= ($i+1) ?> <?= $section_css_class ?>">
  <div class="home-container <?= $container_class ?> page_container">
  <?php if( $content_model == '0' ): ?>
    <div style="color:<?= $color; ?> ">
    <?php if( $section_title != '' ): ?>
      <?php
        $section_title_class = '';
        if ( $section_subtitle == '' ) $section_title_class = 'no-subtitle';
      ?>
      <h1 class="section-title <?= $section_title_class ?>"><a href="<?= $category_link ?>"><?= $section_title ?></a></h1>
    <?php endif; ?>
    <?php if ( $section_subtitle != '' ): ?>
      <div class="section-subtitle"><?= do_shortcode($section_subtitle) ?></div>
    <?php endif;?>
    <!-- team items block -->
    <?php if ( $our_team->have_posts() ): ?>
      <div class="row">
      <?php while ( $our_team->have_posts() ): $our_team->the_post(); ?>
        <div class="col-md-3">
          <div class="magee-animated animated fadeInDown" data-animationduration="0.9" data-animationtype="fadeInDown" data-imageanimation="no" style="visibility: visible; animation-duration: 0.9s;">
            <div class="magee-person-box" id="">
              <div class="person-img-box">
                <div class="img-box figcaption-middle text-center fade-in">
                <?php if ( has_post_thumbnail() ): ?>
                  <a href="<?= the_permalink() ?>"><?php the_post_thumbnail( '', 'style="visibility: visible"' ) ?></a>
                <?php else: ?>
                  <a href="<?= the_permalink() ?>"><img src="<?= get_template_directory_uri() ?>/images/no-image.png" alt="" style="visibility: visible;"></a>
                <?php endif ?>                  
                </div>
              </div>
              <div class="person-vcard text-center">
                <h3 class="person-name" style="text-transform: uppercase;"><a href="<?= the_permalink() ?>"><?php the_title() ?></a></h3>
                <h4 class="person-title" style="text-transform: uppercase;">Супер профи!</h4>
                <div class="text-left">
                  <p class="person-desc"><?php the_excerpt() ?></p>
                </div>                 
              </div>
            </div>
          </div>
        </div>
      <?php endwhile ?>  
      </div>
    <?php else: ?>
      <?= 'Записей нет!' ?>       
    <?php endif ?>     
    </div>
    <?php else: ?>
      <?php if( $section_title != '' ): ?>
        <div class="section-title"><?= do_shortcode($section_title) ?></div>
      <?php endif; ?>
      <div class="home-section-content">
      <?php if ( function_exists('Form_maker_fornt_end_main') ) {
           $section_content = Form_maker_fornt_end_main($section_content);
        }
        echo do_shortcode($section_content);
      ?>
      </div>
    <?php endif; ?>
  </div>
  <div class="clear"></div>
</section>


<?php
// team item layout template
/*
<div class="col-md-3">
  <div class="magee-animated animated fadeInDown" data-animationduration="0.9" data-animationtype="fadeInDown" data-imageanimation="no" style="visibility: visible; animation-duration: 0.9s;">
    <div class="magee-person-box" id="">
      <div class="person-img-box">
        <div class="img-box figcaption-middle text-center fade-in">
          <img src="image" alt="" style="visibility: visible;">
        </div>
      </div>
      <div class="person-vcard text-center">
        <h3 class="person-name" style="text-transform: uppercase;">name</h3>
        <h4 class="person-title" style="text-transform: uppercase;">skills</h4>
        <p class="person-desc">...content</p>
        <ul class="person-social">
          <li>
            <a href="#">
              <i class="fa fa-instagram" style="visibility: visible;"></i>
            </a>
          </li>
          <li>
            <a href="#">
              <i class="fa fa-facebook" style="visibility: visible;"></i>
            </a>
          </li>
          <li>
            <a href="#">
            <i class="fa fa-google-plus" style="visibility: visible;"></i>
          </a>
          </li>
          <li>
            <a href="#">
              <i class="fa fa-envelope" style="visibility: visible;"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
*/
  
// onetone team block realization 
/*
$team_item = '';
$team_str  = '';

for ( $j = 0; $j < 8; $j++ ) {
  $avatar      = esc_url(onetone_option( 'section_avatar_' . $i . '_' . $j ));
  $link        = esc_url(onetone_option( 'section_link_' . $i . '_' . $j ));
  $name        = esc_attr(onetone_option( 'section_name_' . $i . '_' . $j ));
  $byline      = esc_attr(onetone_option( 'section_byline_' . $i . '_' . $j ));
  $description = onetone_option('section_desc_' . $i . '_' . $j);
     
  if ( $avatar != '' ):
    if( $link != '' ) 
      $image = 
       '<a href="' . $link . '" target="_blank">
          <img src="' . $avatar . '" alt="' . $name . '" style="border-radius: 0; display: inline-block; border-style: solid;">
          <div class="img-overlay primary">
            <div class="img-overlay-container">
              <div class="img-overlay-content">
                <i class="fa fa-link"></i>
              </div>
            </div>
          </div>
        </a>';
    else $image = '<img src="'. $avatar . '" alt="">';
    
    $icons = '';
    
    for ( $k = 0; $k < 4; $k++ ) {
      $icon = str_replace('fa-', '', esc_attr(onetone_option('section_icon_' . $i . '_' . $j . '_' . $k )));
      $link = esc_url(onetone_option('section_icon_link_' . $i . '_' . $j . '_' . $k ));
      if ( $icon != '' ) 
        $icons .= 
         '<li>
            <a href="' . $link . '">
              <i class="fa fa-' . $icon . '"></i>
            </a>
          </li>';
    }

    $team_item .= 
     '<div class="col-md-'.$col.'">
        <div class="' . $onetone_animated . '" data-animationduration="0.9" data-animationtype="fadeInDown" data-imageanimation="no">
          <div class="magee-person-box" id="">
            <div class="person-img-box">
              <div class="img-box figcaption-middle text-center fade-in">' . $image .'</div>
            </div>
            <div class="person-vcard text-center">
              <h3 class="person-name" style="text-transform: uppercase;">' . $name . '</h3>
              <h4 class="person-title" style="text-transform: uppercase;">' . $byline . '</h4>
              <p class="person-desc">' . do_shortcode($description) .'</p>
              <ul class="person-social">' . $icons . '</ul>
            </div>
          </div>
        </div>
      </div>';
    $m = $j + 1;
    if ( $m % $columns == 0 ) {
      $team_str .= '<div class="row">' . $team_item . '</div>';
      $team_item = '';
    }
  endif;
}

if ( $team_item != '' ) $team_str .= '<div class="row">' . $team_item . '</div>';
echo $team_str;
*/
?>
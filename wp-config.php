<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'laniakea');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'NJ[%{^rh%%tet8>lvtGUVK|5!jyNwSl>eXcIbwsZwe9:`c8+fxgdhy>7*3;[z?t)');
define('SECURE_AUTH_KEY',  'pPk?x.Fi$J;a-WYx_,kSPGk<qhd|Y[a0er>?+):FC`,TiL[LwOc(>39Orw+obV/D');
define('LOGGED_IN_KEY',    'hR,-!;b}u*1r5TY2-ngoDo)8a7|H=bN@0:p=G&!e24ZF5[xa)}{KT9[K?S6.1$`p');
define('NONCE_KEY',        'Md.TL)am]RLv`=<ab}7oso*/h]~7nAb{4V.$,7,7ahcr5zE?^]I1p7 K)fNq7_A]');
define('AUTH_SALT',        '}(x}m^+I!(75wy1e? [.dR^ r7>)hh?BL0M+~nt}O+u~n0fuvxZqZbBOL%N[>0?y');
define('SECURE_AUTH_SALT', '4NpV=P[pv~U+{N~.L=OZhw9uZ/Ig&^Wz=#[N?!>GE~s&e3:YuAG`Y=A2YE{?mYJu');
define('LOGGED_IN_SALT',   'VoU,dUZp2Z7+xHNW itv9+YSU)r4WE`yuFDW@&<)trGxEu:xAJbWx-L#N#OYLhJ.');
define('NONCE_SALT',       '7GBP[hVypzJYy:oF/)^9gvwSWA~[]6;jdyI5>|Q5tN59-Jq24oq1yulY EF?;-VM');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'lak_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
